from fastai.vision import *

def get_lrg(b):
    if not b:
        raise Exception()
    # x is tuple. e.g.: (array([96 155 269 350]), 16)
    # x[0] returns a numpy array. e.g.: [96 155 269 350]
    # x[0][-2:] returns a numpy array. e.g.: [269 350]. This is the width x height of a bbox.
    # x[0][:2] returns a numpy array. e.g.: [96 155]. This is the x/y coord of a bbox.
    # np.product(x[0][-2:] - x[0][:2]) returns a scalar. e.g.: 33735
    b = sorted(b, key=lambda x: np.product(x[0][-2:] - x[0][:2]), reverse=True)
    return b[0] # get the first element in the list, which is the largest bbox for one image.
def hw_bb(bb):
    return np.array([ bb[1], bb[0], bb[3] + bb[1] - 1, bb[2] + bb[0] - 1 ])
torch.cuda.set_device(0)

PATH = untar_data('pascal_2007')
trn_j = json.load( (PATH / 'train.json').open() )
IMAGES,ANNOTATIONS,CATEGORIES = ['images', 'annotations', 'categories'] 
FILE_NAME,ID,IMG_ID,CAT_ID,BBOX = 'file_name','id','image_id','category_id','bbox'
JPEGS =  '/train'
IMG_PATH = PATH/JPEGS

cats = dict((o[ID], o['name']) for o in trn_j[CATEGORIES])
trn_fns = dict((o[ID], o[FILE_NAME]) for o in trn_j[IMAGES])
trn_ids = [o[ID] for o in trn_j[IMAGES]]

trn_anno = collections.defaultdict(lambda:[])
for o in trn_j[ANNOTATIONS]:
    if not o['ignore']:
        bb = o[BBOX]  # one bbox. looks like '[155, 96, 196, 174]'.
        bb = hw_bb(bb) # output '[96 155 269 350]'.
        trn_anno[o[IMG_ID]].append( (bb, o[CAT_ID]) )

# a is image id (int), b is tuple of bbox (numpy array) & class id (int)
trn_lrg_anno = { a: get_lrg(b) for a, b in trn_anno.items() if (a != 0 and a != 1) }




(PATH / 'tmp').mkdir(exist_ok=True)
CSV = PATH / 'tmp/lrg.csv'

# Pandas
df = DataFrame({ 'fn': [trn_fns[o] for o in trn_ids],
                'cat': [cats[trn_lrg_anno[o][1]] for o in trn_ids] }, columns=['fn', 'cat'])
df.to_csv(CSV, index=False)

# f_model = resnet34
sz=224
bs=64

defaults.device = torch.device('cuda')

tfms = get_transforms()
data = ImageDataBunch.from_csv(PATH, JPEGS,',', CSV, size=224, resize_method=ResizeMethod.SQUISH, bs=bs).normalize()

f_model = models.resnet34

# learn = cnn_learner(data, f_model, metrics=error_rate)
model = simple_cnn((3,224,224, 20))
learn = Learner(data,model)
# learn = cnn_learner(data, models.resnet34, metrics=accuracy)

learn.fit(3, lr=1)

# learn.save('one_epoch')
# img = learn.data.train_ds[0][0]
# print(learn.predict(img))

# learn.export()