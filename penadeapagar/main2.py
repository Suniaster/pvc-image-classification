from fastai.vision import *
path = Path('./data/temp/')
defaults.device = torch.device('cuda')
tfms = get_transforms()
data = ImageDataBunch.from_folder(path,  size=224, resize_method=ResizeMethod.SQUISH, bs=64).normalize()

learn = cnn_learner(data, models.resnet18, metrics=accuracy)
# learn.fit(3)

# learn.save('deu_bom')
img = learn.data.train_ds[0][0]
# print(data.classes)
# print(learn.predict(img))

img.show(figsize=(2, 1), title='Predicting')
plt.show()
# learn.export()