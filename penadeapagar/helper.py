import json
def load_annotations(path):
    with open(path, 'r') as f:
        trn_j = json.load(f)
    IMAGES,ANNOTATIONS,CATEGORIES = ['images', 'annotations', 'categories'] 
    # ctg = trn_j[CATEGORIES]

    FILE_NAME,ID,IMG_ID,CAT_ID,BBOX = 'file_name','id','image_id','category_id','bbox'

    # cats = dict((o[ID], o['name']) for o in trn_j[CATEGORIES])
    # trn_fns = dict((o[ID], o[FILE_NAME]) for o in trn_j[IMAGES])
    # trn_ids = [o[ID] for o in trn_j[IMAGES]]

    # trn_anno = collections.defaultdict(lambda:[])
    # for o in trn_j[ANNOTATIONS]:
    #     if not o['ignore']:
    #         bb = o[BBOX]
    #         bb = np.array([bb[1], bb[0], bb[3]+bb[1]-1, bb[2]+bb[0]-1])
    #         trn_anno[o[IMG_ID]].append((bb,o[CAT_ID]))
    ## labels_t = create_label_list(trn_j)
    return trn_j


def create_label_list(trn_j):
    trn_anno = []
    for o in trn_j['annotations']:
        if not o['ignore']:
            trn_anno.append(o['category_id'])
    return trn_anno