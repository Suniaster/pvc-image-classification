from fastai.vision import *
import cv2
import csv
import xml.dom.minidom

xml1 = 'data/VOCdevkit/VOC2007/Annotations/000001.xml'


def fill_labels(toDo='train'):
    if toDo != 'train' and toDo != 'test':
        print('Argumento invalido para toDo.')
        return

    PATH_IMAGES = './data/VOCdevkit/VOC2007/JPEGImages/'
    PATH_ANNO = './data/VOCdevkit/VOC2007/Annotations/'
    PATH_NEW = './data/VOCdevkit/VOC2007/'
    path_img = Path(PATH_IMAGES)
    path_anno = Path(PATH_ANNO)
    path_new = Path(PATH_NEW)
    rows = []
    
    for xml_label in path_anno.ls():
        doc = xml.dom.minidom.parse(str(xml_label))

        # Pegando todas as labens
        labels = doc.getElementsByTagName("object")
        
        img_id = xml_label.name[:-4]
        img_path = PATH_IMAGES + img_id + '.jpg'
        rel_path = 'JPEGImages/' + img_id + '.jpg'

        # salvar labels duplivadas
        ja_colocado = []
        for label in labels:
            obj = label.getElementsByTagName('name')[0].firstChild.data

            # Pular labels duplicadas para uma mesma imagem
            if obj in ja_colocado:
                continue

            ja_colocado.append(obj)
            new_row = [rel_path, obj]
            rows.append(new_row)
            pass
        pass
    pass

    csvData = [['name', 'label']] + rows
    
    with open(PATH_NEW+'labels.csv', 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)

    csvFile.close()

fill_labels(toDo='test')
fill_labels(toDo='train')