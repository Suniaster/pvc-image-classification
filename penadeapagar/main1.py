from fastai.vision import *
from helper import load_annotations, create_label_list
path = untar_data('pascal_2007')
IMAGES,ANNOTATIONS,CATEGORIES = ['images', 'annotations', 'categories'] 
FILE_NAME,ID,IMG_ID,CAT_ID,BBOX = 'file_name','id','image_id','category_id','bbox'

############ LOADING TRAIN LABELS ############
data_1 = load_annotations('/home/thiago/.fastai/data/pascal_2007/train.json')
data_2 = load_annotations('/home/thiago/.fastai/data/pascal_2007/valid.json')
data_3 = load_annotations('/home/thiago/.fastai/data/pascal_2007/test.json')

anot_1 = data_1[ANNOTATIONS]
anot_3 = data_3[ANNOTATIONS]

labels = anot_1 + anot_3
labels = [x for x in labels if x['ignore'] == 0]
labels = sorted(labels, key= lambda i: i['image_id'])
labels_list = [x['category_id'] for x in labels ]

########### LOADING TEST IMAGES #############

path_train = path/'train'
path_test = path/'test'
img_set1 = sorted(data_1[IMAGES], key= lambda i: i['id'])
img_set2 = sorted(data_2[IMAGES], key= lambda i: i['id'])
img_set3 = sorted(data_3[IMAGES], key= lambda i: i['id'])

images_list = []
for l in labels:
    img_id = l['image_id']
    a=[item for item in img_set1 if item['id'] == img_id]
    b=[item for item in img_set2 if item['id'] == img_id]
    c=[item for item in img_set3 if item['id'] == img_id]
    if len(a) != 0:
        fname = a[0]['file_name']
        path_name = path_train
    elif len(b) != 0:
        fname = b[0]['file_name']
        path_name = path_train
    else:
        fname = c[0]['file_name']
        path_name = path_test
    new_img = str(path_name) +'/'+ fname 
    images_list.append(new_img)

# train_path = Path(path/'train')
# # print(train_path)
# path_imgs = path/'train'
# data_list = []
# for img in path_imgs.ls():
#     pass
#     data_list.append(str(img))
# data_list = sorted(data_list)
# data_list.pop()

# print(len(labels))
# print(len(data_list))


########## RESTO 
tfms = get_transforms()
data = ImageDataBunch.from_lists(path, fnames = images_list, labels=labels_list).\
    transform(tfms).\
    normalize(imagenet_stats)
data.show_batch(rows=3, figsize=(12,9))
# learn = create_cnn(data, models.resnet34, metrics=error_rate)

# learn.fit_one_cycle(4)

# learn.save('clabby-stage-1')

# interp = ClassificationInterpretation.from_learner(learn)

# interp.plot_top_losses(9, figsize=(15, 11))

# print(data.classes)

# model = simple_cnn((20,500,375,20))
# learn = Learner(data, model)

# learn.fit(1)
# learn.recorder.plot_lr(show_moms=True)