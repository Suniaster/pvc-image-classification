import cv2
import os
import numpy as np
from matplotlib import pyplot as plt
from sklearn.feature_extraction.image import extract_patches_2d
import gc
from .Helper import Model_hist

class Kernel_Hist(Model_hist):
    def __init__(self, max_imgs=None, tam_quadrado=5 ,n_clusters=10):
        self._q_size = tam_quadrado
        self.imgs_shape = (100, 100)
        super().__init__(max_imgs=max_imgs, n_clusters = n_clusters)

    ###### Protected Methods ######
    def _process_img(self, img):
        # return img
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        return cv2.resize(img, self.imgs_shape)

    def _select_random_from_list(self, list_i, min_length, max_length):
        cp = list_i
        np.random.shuffle(cp)
        p_range = np.random.randint(min_length, max_length, dtype=int)
        if p_range > len(list_i):
            p_range = len(list_i)-1

        rnd = np.random.randint(0, len(list_i)-p_range, dtype=int)

        return cp[rnd:rnd+p_range]

    ######      Public       ######
    def features(self, img):
        pr_img = self._process_img(img)
        try:
            color = pr_img.shape[2]
        except:
            color = 1
        q = self._q_size 
        feats = []
        i = 0
        j = 0
        while(True):
            if j+q > pr_img.shape[0]:
                i += q
                j=0
            if i+q > pr_img.shape[1]:
                break
            feat = pr_img[j:j+q, i:i+q]

            feats.append(feat.reshape(q*q*color))

            j += q
        return self._select_random_from_list(feats, 340, 360)
        # return feats


class Sift_Hist(Model_hist):
    def __init__(self, max_imgs=None, n_clusters=10):
        super().__init__(max_imgs=max_imgs, n_clusters = n_clusters)

    def _process_img(self, img):
        return None
    
    def _select_random_from_list(self, list_i, min_length, max_length):
        cp = list_i
        np.random.shuffle(cp)
        p_range = np.random.randint(min_length, max_length, dtype=int)
        if p_range > len(list_i):
            p_range = len(list_i)-1

        rnd = np.random.randint(0, len(list_i)-p_range, dtype=int)

        return cp[rnd:rnd+p_range]

    def features(self, img):
        sift = cv2.xfeatures2d.SIFT_create()
        _, feats = sift.detectAndCompute(img,None)
        return feats[0:400]

    def _create_feat_list(self):
        # all feats deve armazenar um vetor de uma unica dimensão com 
        # \todos os features de todas as imagens
        all_feats = self.features(self.data.train_imgs[0])

        # img_feats é uma matriz de n_imgs por n_feats com cada linha contendo todas as features dessa imagem
        img_feats = [len(all_feats)]
        i=True
        for img in self.data.train_imgs:
            if i :
                i=False
                continue
            fs_find = self.features(img)
            all_feats = np.concatenate((all_feats,self.features(img)),axis=0)
            # all_feats+=fs_find
            img_feats.append(len(fs_find))
        gc.collect()
        self.data.train_img_feats_len = img_feats
        self.data.train_all_feats = all_feats
        print(all_feats.shape)
        return all_feats 