import cv2
from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans
from sklearn.model_selection import GridSearchCV
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler
import os
import numpy as np
from pathlib import Path
import glob
from matplotlib import pyplot as plt
import sklearn
import pickle
import csv
import gc

class data:
    
    def __init__(self, max_imgs=None, classes=['0','1']):
        self.classes = classes
        self.max_imgs = max_imgs
    
    #######  Protected Methods #####
    def _get_ids_labels(self, clas, tset):
        gt_ids = []
        labels_cls = []

        path = "./data/VOCdevkit/VOC2007/ImageSets/Main/{}_{}.txt".format(clas, tset)
        total = 0 # var de contagem
        ## Pega os id's e os labels das classes desse set
        with open( path , "r") as handle:
            for line in handle:
                img, label = line.split()
                gt_ids.append(img)
                if(int(label) == 1):
                    labels_cls.append(1)
                else:
                    labels_cls.append(0)
                total +=1
                if self.max_imgs != None:
                    if total > self.max_imgs:
                        break
        return [gt_ids, labels_cls]
    
    def _load_train(self, clas):
        ret = self.get_images(clas, 'trainval')
        self.train_imgs = ret[0]
        self.train_labels = ret[1]
        return ret

    def _load_test(self, clas):
        ret = self.get_images(clas, 'test')
        self.test_imgs = ret[0]
        self.test_labels = ret[1]
        return ret

    #######  Public Methods  #####
    def get_images(self, clas,tset):

        # Pegando id's corretos e labels de cada id
        gt_ids, labels_cls = self._get_ids_labels(clas, tset)

        ## Pega os id's e os labels das classes desse set
        img_list = []
        path_img = './data/VOCdevkit/VOC2007/JPEGImages/'

        if self.max_imgs != None:
            total = 0
            for name in gt_ids:
                # pegando img
                fname = path_img + name +'.jpg'
                img = cv2.imread(fname,-1)
                
                # salvando
                img_list.append(img)
                
                total += 1
                if total > self.max_imgs:
                    break
        else:
            for name in gt_ids:
                # pegando img
                fname = path_img + name +'.jpg'
                img = cv2.imread(fname, -1)

                # salvando
                img_list.append(img)

        return [img_list, labels_cls] 

    def find_class_id(self, str_class):
        classes = self.classes
        i=0
        for cls in classes:
            if cls == str_class:
                return i
            i+=1
        return -1

    def ler_todas_imgs(self,clas, tset):
        gt_ids = []
        gt = []

        ## Pega os id's e os labels das classes desse set
        with open("./data/VOCdevkit/VOC2007/ImageSets/Main/{}_{}.txt".format(clas, tset), "r") as handle:
            for line in handle:
                img, label = line.split()
                gt_ids.append(img)

        ## Pega os id's e os labels das classes desse set
        img_list = []
        path_img = './data/VOCdevkit/VOC2007/JPEGImages/'

        for name in gt_ids:
            fname = path_img + name +'.jpg'
            img = cv2.imread(fname, -1)
            img_list.append(img)
        return img_list

class Model_hist:
    def __init__(self, max_imgs=None, n_clusters=10):
        # Inicializando variaveis de entrada
        self.n_clusters = n_clusters
        self.max_imgs =max_imgs

        self.kmeans = None
        classes =  ['aeroplane', 'bicycle', 'bird', 'boat',
        'bottle', 'bus', 'car', 'cat', 'chair', 'cow',
        'diningtable', 'dog', 'horse', 'motorbike',
        'person', 'pottedplant', 'sheep', 'sofa', 'train',
        'tvmonitor'
        ]
        self.data = data(max_imgs=max_imgs, classes=classes)

    ####### Metodos abstratos  ######
    def features(self,img):
        raise NotImplementedError

    def _process_img(self, img):
        raise NotImplementedError

    #######  Protected Methods #####
    def _hist_from_kmeans(self,kmeans_ret):
        hist = np.histogram(kmeans_ret,bins=np.arange(self.n_clusters+1), density=True)[0]
        return hist

    def _create_feat_list(self):
        # all feats deve armazenar um vetor de uma unica dimensão com 
        # \todos os features de todas as imagens
        all_feats = []

        # img_feats é uma matriz de n_imgs por n_feats com cada linha contendo todas as features dessa imagem
        img_feats = []
        
        for img in self.data.train_imgs:
            fs_find = self.features(img)
            # all_feats = np.concatenate((all_feats,self.features(img)),axis=0)
            all_feats+=fs_find
            img_feats.append(len(fs_find))
        
        gc.collect()
        self.data.train_img_feats_len = img_feats
        self.data.train_all_feats = all_feats
        return all_feats

    def _fit_svm(self):
        print("~>SVC: Criando modelo")

        # self.clf = sklearn.svm.SVC(probability=True, verbose=True)
        param_grid = {'C': [0.01,0.1, 1, 10, 100], 'gamma': [100, 1, 0.1, 0.01, 0.001, 0.00001, 10]}
        self.clf = GridSearchCV(sklearn.svm.SVC(probability=True, verbose=True), param_grid)

        self.clf.fit(self.world_hist, self.data.train_labels)

    def _create_hists(self):
        print("~>Hist: Achando features das imagens")
        f_list = self._create_feat_list()

        print("~>Hist: Fazendo KMeans ")

        #Caso nenhum kmeans tenha sido carreado, é computado um novo
        if self.kmeans == None:
            self.kmeans = MiniBatchKMeans(n_clusters = self.n_clusters ,batch_size=int(len(f_list)*0.15), max_iter=2000)
            self.kmeans_predict =self.kmeans.fit(f_list)
        
        # Pegando vetor de predição dos dados de treino
        self.kmeans_predict = self.kmeans.predict(f_list)

        print("~>Hist: Criando Histogramas ")
        self.world_hist = []
        old_count = 0
        quantidade_imgs = len(self.data.train_imgs)

        # Para cada uma das imagens é calculado seu respectivo histograma
        # salvando-o na posição correspondente no vetor hist
        for i in range(quantidade_imgs):
            l = self.data.train_img_feats_len[i]

            # Criando o histograma correto para a imagem
            h = self.kmeans_predict[old_count:old_count+l]
            new_hist = self._hist_from_kmeans(h)
            
            # Salvando resultado
            self.world_hist.append(new_hist)
            old_count += l

    #######  Public Methods  #####

    def fit(self):
        if self.clf == None or self.kmeans == None:
            self._create_hists()
            self._fit_svm()
    
    def predict(self, img):

        # Pegando seus features e fazendo histograma
        img_feats = self.features(img)
        km_ret = self.kmeans.predict(img_feats)
        hist = self._hist_from_kmeans(km_ret)

        # Reshape para dar predict em uma só amostra
        toPred = hist.reshape(1,-1)

        this_cls = self.clf.predict(toPred)[0]
        pass_ = None
        return [[this_cls], pass_ ,self.clf.predict_proba(toPred)]

    def test_model(self, exp_id, cls, tset, rewrite=False):

        fname = "./data/VOCdevkit/results/VOC2007/Main/{}_cls_{}_{}.txt".format(exp_id, tset, cls)

        # Caso o arquivo desejado já exista, não é necessario fazer nada
        exists = os.path.isfile(fname)
        if exists and rewrite == False:
            return

        gt_ids = []
        gt = []
        ## Pega os id's e os labels das classes desse set
        with open("./data/VOCdevkit/VOC2007/ImageSets/Main/{}_{}.txt".format(cls, tset), "r") as handle:
            for line in handle:
                img, label = line.split()
                gt_ids.append(img)
                gt.append(int(label))

        # Variaveis auxiliares
        path_img = Path('./data/VOCdevkit/VOC2007/JPEGImages/')
        class_id = self.data.find_class_id(cls)

        if class_id == -1:
            print('classe não encontrada')
            return

        # apagando conteudo do arquivo
        open(fname,"w").close()
        handle = open(fname, "a+")
        # Para cada um dos ids do arquivo lido
        for tid in gt_ids:

            img_ = cv2.imread(str(path_img/tid)+'.jpg', -1)
    
            pred = self.predict(img_)
            
            # pega a certeza dessa previsão

            certain = float(pred[2][0][1])

            text = str(tid) + ' ' + str(certain)+'\n'
            handle.write(text)
        handle.close()



    ## Model data:
    def save_kmeans(self, path, fname):
        pickle.dump(self.kmeans, open(path+fname, 'wb'))

    def load_kmeans(self, path, fname):
        self.kmeans = pickle.load(open(path+fname, 'rb'))
    

    def save_clf(self, path, fname, clas):
        pickle.dump(self.clf, open(path+fname+"_"+clas, 'wb'))

    def load_clf(self, path, fname, clas):
        self.clf = pickle.load(open(path+fname+"_"+clas, 'rb'))
    
    def load_model(self, path, fname, clas, train_again):
        if train_again:
            self.kmeans = None
            self.clf = None
        else:
            print("~> Carregando arquivos pre-treinado (caso existam)")
            try: 
                self.load_kmeans('./data/kmeans/', fname)
            except:
                self.kmeans = None
            try:
                self.load_clf('./data/kmeans/', fname, clas)
            except:
                self.clf = None

    def save_model(self, path, fname, clas):
        pickle.dump(self.kmeans, open(path+fname, 'wb'))
        pickle.dump(self.clf, open(path+fname+"_"+clas, 'wb'))
