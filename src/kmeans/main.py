import cv2
from sklearn.cluster import KMeans
from sklearn.neighbors import NearestNeighbors
import sklearn
from matplotlib import pyplot as plt

from .Derivates import Kernel_Hist
from .Derivates import Sift_Hist
from .pedro import get_AP
import datetime
import gc

def main_kmeans(rewrite=False, train_again=False, test=False):
    classes =  ['aeroplane', 'bicycle', 'bird', 'boat',
        'bottle', 'bus', 'car', 'cat', 'chair', 'cow',
        'diningtable', 'dog', 'horse', 'motorbike',
        'person', 'pottedplant', 'sheep', 'sofa', 'train',
        'tvmonitor'
    ]


    ############## Iterando pelas classes ###############
    for cl in classes:

        ############## Lendo arquivos ############### 
        vocab_len = 500
        
        if False:
            model =  Sift_Hist(n_clusters=vocab_len)
            model_fname = 'sift_{}'.format(vocab_len)
            expName = model_fname
        else:
            model =  Kernel_Hist(tam_quadrado = 5, n_clusters=vocab_len)
            model_fname = 'kmeans_5_{}'.format(vocab_len)
            expName = model_fname
        
        # carregando arquivos pretreinados caso existam
        model.load_model('./data/kmeans/', model_fname, cl, train_again)

        currentDT = datetime.datetime.now()
        time_ = currentDT.strftime("%H:%M:%S")
        print("Processando: {}... start: {}".format(cl, time_) )

        model.data._load_train(cl)
        model.fit()
        
        # Coletar garbage pra evitar memoria morrer
        model.data.train_imgs = None
        model.data.train_labels = None
        model.data.train_img_feats_len =None
        model.data.train_all_feats = None
        gc.collect()
        model.save_model( './data/kmeans/', model_fname, cl)
        model.data._load_test(cl)
        if test:
            test_imgs(model.data.test_imgs, model)
            break
        else:
            print("~> Escrevendo arquivos de saida")
            model.test_model(expName, cl, 'test', rewrite=rewrite)

            # print("~> Calculando AP")
            # get_AP(expName, cl, 'test')

        del model 
        gc.collect()
        currentDT = datetime.datetime.now()
        time_ = currentDT.strftime("%H:%M:%S")
        print("Fim do processamento de {} em {}".format(cl, time_))

def test_imgs(imgs, model):
    i=0
    for img in imgs:
        cv2.imshow('imagem',img)

        pred = model.predict(img)
        print(pred)
        i+=1
        print("Pressione Q para finalizar, qualquer outra coisa para continuar...")
        if cv2.waitKey(0) & 0xFF == ord('q'):
            break
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main_kmeans()