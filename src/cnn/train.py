from fastai.vision import *



def train_model(name):
    torch.cuda.set_device(0)
    PATH_NEW = './data/VOCdevkit/VOC2007'
    path = Path(PATH_NEW)

    data = ImageDataBunch.from_csv(path, csv_labels='labels_train.csv' ,size=224, resize_method=ResizeMethod.SQUISH, bs=64).normalize()

    # resnet18
    learn = cnn_learner(data, models.resnet34 , metrics=accuracy)

    # alexnet
    # learn = cnn_learner(data, models.alexnet , metrics=accuracy)

    # Simple cnn
    # model = simple_cnn((3,224,244,20))
    # learn = Learner(data, model)
    # learn.metrics=[accuracy]

    learn.fit(1)

    learn.save(name)
    learn.export(name)

if __name__ == '__main__':
    train_model('resnet34')