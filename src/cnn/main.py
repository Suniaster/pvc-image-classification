from .pedro import get_AP
from .test_model import test_model
from .models.test_learner import load_mylearner
from .train import train_model
from .create_csv_labels import fill_labels
import sys



def force_train(learn_name):
    print("~> Treinando Modelo "+ learn_name)
    train_model(learn_name)
    learn = load_mylearner(learn_name)
    return learn

def try_load(learn_name):
    try:
        print("~> Carregando CNN treinada")
        learn = load_mylearner(learn_name)
    except:
        print("~> CNN pré-treinada não encontrada, começando treinamento")
        train_model(learn_name)
        learn = load_mylearner(learn_name)
    return learn


def main_cnn(rewrite=False, train_again=False):
    # Pegando flags de entrada
    rw = False
    train_again = False
    for arg in sys.argv:
        if arg == '--rewrite':
            print("Certeza que deseja reescrever arquivos de resultado?")
            print("Processo pode ser muito demorado...")
            inp = input("y / n :")
            if inp == "y":
                rw = True
        if arg == '--force-train':
            print("Certeza que deseja forçar treinamento?")
            print("Processo pode ser muito demorado...")
            inp = input("y / n :")
            if inp == "y":
                train_again = True

    ## Cria os labels csv para usar na criação da cnn
    print("~> Criando arquivos csv de labels")
    fill_labels('test')
    fill_labels('train')

    learn_name = 'resnet34'

    # Carregando modelo de aprendizado
    if train_again:
        learn = force_train(learn_name)
    else:
        learn = try_load(learn_name)

    exp_id = learn_name

    print("~> Verificando resultados...")
    for cls in learn.data.classes:
        # cria o arquivo txt com os ids das imagens e a
        # confiança dqa imagem pertencer a classe
        if rw:
            print("~> Escrevendo arquivo de saida")
        test_model(exp_id, cls, 'test', learn, rewrite=rw)

        # calcula o average precision
        get_AP(exp_id, cls, 'test')

if __name__ == "__main__":
    main_cnn()