from fastai.vision import *
import cv2
import csv
import xml.dom.minidom

xml1 = 'data/VOCdevkit/VOC2007/Annotations/000001.xml'


def fill_labels(toDo='train'):
    if toDo != 'train' and toDo != 'test':
        print('Argumento invalido para toDo.')
        return

    PATH_IMAGES = './data/VOCdevkit/VOC2007/JPEGImages/'
    PATH_labels = './data/VOCdevkit/VOC2007/ImageSets/Main/'
    PATH_SAVE = './data/VOCdevkit/VOC2007/'
    
    # path para as imagem relativo ao csv
    PATH_IMG_REL_CSV = 'JPEGImages/'

    path_img = Path(PATH_IMAGES)
    path_labels = Path(PATH_labels)
    path_save = Path(PATH_SAVE)
    rows = []

    classes =  ['aeroplane', 'bicycle', 'bird', 'boat',
        'bottle', 'bus', 'car', 'cat', 'chair', 'cow',
        'diningtable', 'dog', 'horse', 'motorbike',
        'person', 'pottedplant', 'sheep', 'sofa', 'train',
        'tvmonitor'
    ]

    for cls in classes:
        fname = "{}_{}.txt".format(cls, toDo)
        f = open(PATH_labels + fname, 'r')
        fl = f.readlines()

        for line in fl:
            _id, isfrom = [x for x in line.split(' ') if x != '']
            if isfrom == '1\n':
                name = PATH_IMG_REL_CSV+"{}.jpg".format(_id)
                rows.append([name, cls])
        f.close()

    csvData = [['name', 'label']] + rows
    
    with open(PATH_SAVE+'labels_{}.csv'.format(toDo), 'w') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(csvData)

    csvFile.close()


if __name__ == '__main__':
    fill_labels(toDo='test')
    fill_labels(toDo='train')