import numpy as np
import matplotlib.pyplot as plt
from fastai.vision import *
import PIL
import os

from .models.test_learner import load_mylearner
from .pedro import get_AP

def test_model(exp_id, cls, tset, learner, rewrite=False):

    fname = "./data/VOCdevkit/results/VOC2007/Main/{}_cls_{}_{}.txt".format(exp_id, tset, cls)

    # Caso o arquivo desejado já exista, não é necessario fazer nada
    exists = os.path.isfile(fname)
    if exists and rewrite == False:
        return

    gt_ids = []
    gt = []
    ## Pega os id's e os labels das classes desse set
    with open("./data/VOCdevkit/VOC2007/ImageSets/Main/{}_{}.txt".format(cls, tset), "r") as handle:
        for line in handle:
            img, label = line.split()
            gt_ids.append(img)
            gt.append(int(label))

    # Variaveis auxiliares
    classes = learner.data.classes
    path_img = Path('./data/VOCdevkit/VOC2007/JPEGImages/')
    class_id = find_class_id(classes, cls)

    if class_id == -1:
        print('classe não encontrada')
        return

    # apagando conteudo do arquivo
    open(fname,"w").close()

    # Para cada um dos ids do arquivo lido
    for tid in gt_ids:
        # Abre a imagem
        img = PIL.Image.open(str(path_img/tid)+'.jpg')
        # cria um objeto que pode ser usado pelo fast ai
        img_fast = Image(pil2tensor(img, dtype=np.float32).div_(255))

        # faz a previsão
        # pred é um array que tem:
        # [0] classe com maior porcentagem de certeza
        # [1] o id dessa classe
        # [2] array com a certeza de todas as classe
        # ex: (Category dog, tensor(11), tensor([2.0864e-04, 1.8267e-03, 1.1290e-03, 4.0568e-04, 7.9123e-03, 3.0915e-04,
        # 2.2168e-03, 8.2916e-03, 3.3025e-03, 3.0226e-04, 2.1372e-03, 5.4135e-01,
        # 1.7575e-03, 6.1470e-05, 3.8388e-01, 2.0623e-03, 7.0510e-03, 1.8300e-02,
        # 1.8020e-04, 1.7322e-02]))
        pred = learner.predict(img_fast)

        # pega a certeza dessa previsão
        certain = float(pred[2][class_id])
        with open(fname, "a+") as handle:
            text = str(tid) + ' ' + str(certain)+'\n'
            handle.write(text)
        handle.close()

def find_class_id(classes, str_class):
    i=0
    for cls in classes:
        if cls == str_class:
            return i
        i+=1
    return -1

if __name__ == '__main__':

    rn34 =  load_mylearner('resnet18')

    test_model('test_rn34', 'person', 'test', rn34)

    get_AP('test_rn34', 'person', 'test')
