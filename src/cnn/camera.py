from pedro import get_AP
from test_model import test_model
from models.test_learner import load_mylearner
import cv2
import numpy as np
import matplotlib.pyplot as plt
from fastai.vision import *
import PIL
import os


def print_classes(pred, classes):
    i=0
    for cls in classes:
        if pred[2][i] > 0.12:
            print("{}: {}".format(cls, float(pred[2][i])))
        i+=1
    print('----')

learn = load_mylearner('resnet152')

cap = cv2.VideoCapture(0)
while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    img = PIL.Image.fromarray(frame)
    img_fast = Image(pil2tensor(img, dtype=np.float32).div_(255))
    

    pred = learn.predict(img_fast)

    print_classes(pred, learn.data.classes)
    
    # # Display the resulting frame
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()