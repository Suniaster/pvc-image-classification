import numpy as np
import matplotlib.pyplot as plt

from fastai.vision import *


def load_mylearner(name=''):
    PATH_TRAIN = './data/VOCdevkit/VOC2007/'
    path_train = Path(PATH_TRAIN)
    if name == '':
        name = 'export.pkl'
    learn = load_learner(path_train, name)
    return learn


def test_learner(name):
    # get path to test dir
    PATH = './data/VOCdevkit/VOC2007/'
    path = Path(PATH)
    
    # Load learner
    learn = load_mylearner(name)
    
    # Load data to test
    data = ImageDataBunch.from_csv(path, csv_labels='labels_test.csv' ,size=224, resize_method=ResizeMethod.SQUISH, bs=64).normalize()
    
    # iterate through data set
    classes = learn.data.classes
    for test in data.train_ds:
        img = test[0]
        pred = learn.predict(img)
        # print result in terminal
        print_preds(pred, classes, 0.1)
        
        img.show(figsize=(10, 5), title=' / '+str(test[1]))
        plt.show()
        print('-------')


def print_preds(pred, classes, lim):
    for i in range(len(classes)):
        to_aval = float(pred[2][i])
        if(to_aval > lim):
            print(classes[i] + ': ' + str(to_aval))
    print('-------')


if __name__ == '__main__':
    test_learner('resnet18')