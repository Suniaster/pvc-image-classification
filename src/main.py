#from cnn.main import main_cnn

from kmeans.main import main_kmeans
import sys



if __name__ == '__main__':
    # Pegando flags de entrada
    rw = False
    train_again = False
    run_kmeans = False
    run_cnn = False
    test = False
    for arg in sys.argv:
        if arg == '--rewrite':
            print("Certeza que deseja reescrever arquivos de resultado?")
            print("Processo pode levar MUITO tempo...")
            inp = input("[y/n]: ")
            if inp == "y":
                rw = True
        if arg == '--force-train':
            print("Certeza que deseja forçar treinamento?")
            print("Processo pode levar MUITO tempo...")
            inp = input("[y/n]: ")
            if inp == "y":
                train_again = True

        if arg == '--teste':
            test = True
        if arg == '-cnn':
            pass
            #run_cnn = True
        if arg == '-kmeans':
            run_kmeans = True


    if run_cnn == True: 
        main_cnn(rewrite=rw, train_again=train_again)
    if run_kmeans == True:
        main_kmeans(rewrite=rw, train_again=train_again, test=test)