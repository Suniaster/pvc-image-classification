# PVC -  Reconhecimento de Classes

Trabalho 4 da matéria de Principios da visão computacional 2019/1
Feito por Thiago Chaves Monteiro & Larissa Andrade
Link do repositório: https://gitlab.com/Suniaster/pvc-projeto1

## Requisitos

Para o uso do progama é necessário que a instalação do software python na versão 3.7.3 tenha sido feito na maquina, a biblioteca opencv para python na versão 4.0.0 e o framework fastai na versão 1.0.52.

É recomendado o uso de anaconda para a instalação do fastai. Para a instalação da conda segue-se o link: https://www.anaconda.com/distribution/.

Para fazer a instalação do fastai tendo conda, rode no terminal:

    conda install -c pytorch -c fastai fastai

Vale notar que essa instalação é dependente da existencia de uma GPU no computador assim como o driver CUDA. Caso não seja possível fazer essas instalações, segue-se o link: https://github.com/fastai/fastai/blob/master/README.md#installation.

Para a instalação do opencv 4:

    pip install opencv-contrib-python

ou

    conda install -c menpo opencv

Além disso é necessario que os arquivos do VOCdevkit sejam baixados, para que o software use-os como base para treinamento. Link: host.robots.ox.ac.uk/pascal/VOC/voc2007/.


## Especificações

O programa é divido em 2 módulos diferentes para a resolução do mesmo problema. Na pasta `src/` os módulos responsáveis pela lógica de cada diferente soluçao estão separadas nas pastas com o nome os nomes de cada um dos mesmos.

Para os scripts funcionarem corretamente deve-se existir todos os arquivos do VOCDevkit dentro da pasta `data/`. Além disso deve-se existir a pasta `data/kmeans/` que será usada para armazenar os arquivos de modelos treinados.

Dessa forma, a estrutura geral desse modelo deve seguir o seguinte:

```this-dir
dir
└─── data
│     └─── kmeans
│     └─── VOCdevkit
│           └─── local
│           │      └─── VOC2006
│           │      └─── VOC2007
│           └─── results
│           │     └─── VOC2006
│           │     └─── VOC2007
│           │           └─── Main
│           └─── VOC2007
│           │     └─── JPEGImages
│           │     └─── ImageSets
│           │           └─── Main
│           └─── VOCcode
│
└───src
    └─── cnn
    └─── kmeans  
    │   main.py

```

## Utilização

O script responsável pela execução do programa é: `src/main.py`. Para executar o módulo que utiliza CNN (Redes Neurais Convolucionais), deve-se passar a flag `-cnn` na linha de execução, e para executar o módulo de Kmeans `-kmeans`.

Além disso, é opcional a utilização das flags `--rewrite` para reescrever os arquivos de resultado e `--force-train` para refazer o treinamento de um dos modelos caso eles já existam. Recomenda-se não utilizar essas duas flags já foi feito o treinamento e/ou salvado seus resultados, visto que esses dois processos levam um grande periodo de tempo.

Exemplos de execução:

```bash
python src/main.py -cnn --force-train
```

```bash
python src/main.py -kmeans --rewrite
```

Vale notar que é possível a execução dos dois modelos com uma unica linha de comando, porém as flags passadas são passadas para as duas.

```bash
python src/main.py -cnn -kmeans --force-train --rewrite
```

### Modulo extra

Para capturar imagens na camera e classifica-las em tempo real, mostrando o resultado no terminal a cada frame:

```bash
python src/cnn/camera.py
```